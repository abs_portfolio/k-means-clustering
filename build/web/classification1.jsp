<%-- 
    Document   : classification1
    Created on : May 21, 2018, 1:12:49 AM
    Author     : OneLove
--%>

<%@page import="source.Country"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Web Programming 2018</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <link rel="stylesheet" href="style/auto.css">


        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>  
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

        <script type="text/javascript">
            google.charts.load('current',
                    {
                        'packages': ['geochart'],
                        // This is a test developers key sudjested by google.
                        'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
                    }
            );

            $(document).ready(function () {
                open_loader();
                setInterval(function () {
                    myFun();
                }, 5000);
                w3_close();
            });
            function open_loader(){
               document.getElementById("regions_div").innerHTML = '</br></br></br></br></br> <img src="./pictures/loader.gif" alt="Be patient..." height="150" width="150"/>';
               document.getElementById("somediv").innerHTML = ' </br></br> </br></br></br> <img src="./pictures/loader.gif" alt="Be patient..." height="150" width="150" />';
            }
            function close_loader(){
               document.getElementById("regions_div").innerHTML = '';
               document.getElementById("somediv").innerHTML = ' ';
            }

            function myFun() {

                var Combined = new Array();
                var count = 0;
                $.ajax({
                    url: "AjaxServlet",
                    success: function (responseJson) {
                        var team, centerLatA, centerLngA, centerLatB, centerLngB,c;
                        $.each(responseJson, function (index, data) {    // Iterate over the JSON array.
                            //console.log(data.count);
                            c = data.count; 
                            if (!isNaN(parseInt(data.team))) {
                                team = parseInt(data.team);
                            }
                            var vals = [];
                            for (var item of data.listA) {
                                vals.push(item.country);
                            }
                            for (var i = 0; i < vals.length; i++) {
                                Combined[count] = [vals[i], team];
                                count++;
                            }
                            if (parseInt(team) === 100) {
                                if (!isNaN(parseInt(data.centerLatA))) {
                                    centerLatA = parseFloat(data.centerLatA);
                                }
                                if (!isNaN(parseInt(data.centerLngA))) {
                                    centerLngA = parseFloat(data.centerLngA);
                                }
                            } else {
                                if (!isNaN(parseInt(data.centerLatA))) {
                                    centerLatB = parseFloat(data.centerLatA);
                                }
                                if (!isNaN(parseInt(data.centerLngA))) {
                                    centerLngB = parseFloat(data.centerLngA);
                                }
                            }
                        });
                        console.log(c);
                        document.getElementById('count').innerHTML = c;
                        //google.charts.setOnLoadCallback(drawMe(Combined));
                        drawMe(Combined, centerLatA, centerLngA, centerLatB, centerLngB);
                    }
                });
            }
            function drawMe(Combined, centerLatA, centerLngA, centerLatB, centerLngB) {
                close_loader();
                drawMap(Combined);
                drawRegionsMap(centerLatA, centerLngA, centerLatB, centerLngB);
            }
            function drawMap(Combined) {
                console.log(Combined);
                var data = new google.visualization.DataTable();
                // Add columns
                data.addColumn('string', 'Country');
                data.addColumn('number', 'Team');

                // Add empty rows
                data.addRows(Combined.length);
                for (var i = 0; i < Combined.length; i++) {
                    var cube = Combined[i];
                    for (var j = 0; j < cube.length; j++) {
                        data.setCell(i, j, cube[j]);
                    }
                }
                var options = {colorAxis: {colors: ['green', 'blue']}};
                options['dataMode'] = 'regions';
                options['width'] = 500;
                options['height'] = 400;

                var geomap = new google.visualization.GeoChart(document.getElementById('somediv'));

                geomap.draw(data, options);
            }



            function drawRegionsMap(centerLatA, centerLngA, centerLatB, centerLngB) {
                var data = new google.visualization.DataTable();
                data.addColumn('number', 'Lat');
                data.addColumn('number', 'Long');
                data.addColumn('number', 'Value');
                data.addColumn({type: 'string', role: 'tooltip'});
                console.log("LongA : " + centerLngA + " centerLatA = " + centerLatA);
                console.log("LongA : " + centerLngB + " centerLatA = " + centerLatB);
                //data.addRows([[parseFloat(2,3), 0, 'tooltip']]);
                data.addRows([[centerLatA, centerLngA, 0, 'greanTeam']]);
                data.addRows([[centerLatB, centerLngB, 0, 'blueTeam']]);

                var options = {
                    colorAxis: {minValue: 0, maxValue: 0, colors: ['#6699CC']},
                    legend: 'none',
                    backgroundColor: {fill: 'transparent', stroke: '#FFF', strokeWidth: 0},
                    datalessRegionColor: '#f5f5f5',
                    displayMode: 'markers',
                    enableRegionInteractivity: 'true',
                    resolution: 'countries',
                    sizeAxis: {minValue: 1, maxValue: 1, minSize: 5, maxSize: 5},
                    region: 'world',
                    keepAspectRatio: true,
                    width: 500,
                    height: 400,
                    tooltip: {textStyle: {color: '#444444'}}
                };

                //var options = {colorAxis: {colors: ['green', 'blue']}};
                var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

                chart.draw(data, options);
            }



        </script>
    </head>
    <body>

        <!-- Top container -->
        <div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
            <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
            <a href="logout.jsp" ><span  class="w3-bar-item w3-right">Log out</span> </a>
        </div>

        <!-- Sidebar/menu -->
        <nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px; background-color:#353434 !important; " id="mySidebar"><br>
            <div class="w3-container w3-row">
                <div class="w3-col s4">
                    <img src="pictures/university_logo.PNG" class="w3-circle w3-margin-right" style="width:46px">
                </div>
                <div class="w3-col s8 w3-bar">
                    <span>Welcome, <strong>hi</strong></span><br>
                    </div>
            </div>
            <hr>
            <div class="w3-container">
                <h5>Dashboard</h5>
            </div>
            <div class="w3-bar-block">
                <a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Close Menu</a>
                <a href="autocompleteEx.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-bank fa-fw"></i>  General</a>
                <!--<a href="classification2.jsp" class=" w3-bar-item w3-button w3-padding"><i class="fa fa-users fa-fw"></i>  Classification</a>-->
                <!--<a href="dataSelect.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i>  Charts</a>-->
                <a href="classification1.jsp" class="w3-bar-item w3-button w3-padding w3-blue"><i class="fa fa-bullseye fa-fw"></i>  K-Means Clustering</a>               
            </div>
        </nav>


        <!-- Overlay effect when opening sidebar on small screens -->
        <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

        <!-- !PAGE CONTENT! -->
        <div class="w3-main" style="margin-left:300px;margin-top:43px;">

            <!-- Header -->
            <header class="w3-container" style="padding-top:22px">
                <div class="w3-container w3-row">
                    <h5><b><img src="pictures/university_logo.PNG" class="w3-circle w3-margin-right" style="width:46px"> My Dashboard</b></h5>
                    <hr>
                </div>
            </header>



            <div class="w3-panel">
                <div class="w3-row-padding" style="margin:0 -16px">
                    <div class="w3-container">
                       
                            <div> <h5 class="div-left">Google GeoChart with the center markers</h5></div>
                       
                        <div>
                        <h5 class="div-roght">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Google GeoChart with the <font color="blue">TEAM-A</font> & <font color="green">TEAM-B</font></h5>
                        </div>
                       
                        
                        <div id="regions_div" class="div-left" style="display:table-cell; vertical-align:middle; text-align:center; width: 600px; height: 400px;" >
                            
                        </div>
                            
                        <div id="somediv" class="div-right" style="display:table-cell; vertical-align:middle; text-align:center;  width: 600px; height: 400px;">
                        </div>
                    </div>
                </div>
                <!-- End page content -->
            </div>
            <hr>
            <h5 class="div-left">Times run algorithm : </h5>
            <h5 id ="count"></h5>
            
        </div>
        <script>
            // Get the Sidebar
            var mySidebar = document.getElementById("mySidebar");

            // Get the DIV with overlay effect
            var overlayBg = document.getElementById("myOverlay");

            // Toggle between showing and hiding the sidebar, and add overlay effect
            function w3_open() {
                if (mySidebar.style.display === 'block') {
                    mySidebar.style.display = 'none';
                    overlayBg.style.display = "none";
                } else {
                    mySidebar.style.display = 'block';
                    overlayBg.style.display = "block";
                }
            }

            // Close the sidebar with the close button
            function w3_close() {
                mySidebar.style.display = "none";
                overlayBg.style.display = "none";
            }
        </script>

    </body>
</html>
