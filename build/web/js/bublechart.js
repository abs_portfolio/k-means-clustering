 google.charts.load('current', {packages: ['corechart']});
 
  function getData() {
                $.ajax({

                    url: "AjaxClassification2",
                    data: {countries: $('#countries').val(), att: $('#attribute').val()},
                    dataType: "JSON",
                    success: function (data) {
                        console.log(data);
                        drawChart(data);
                    }
                });
            }
            function drawChart(jsonData) {
                // Define the chart to be drawn.
                var data = new google.visualization.DataTable();
                var att = $('#attribute').val();
                data.addColumn('string', 'Country');
                data.addColumn('number', att[0].toString());
                data.addColumn('number', att[1].toString());
                data.addColumn('string', 'Team');
                data.addColumn('number', 'Population');
                var team;
                $.each(jsonData, function (i, obj) {
                    if (!isNaN(parseInt(obj.team))) {
                        team = parseInt(obj.team);
                    }
                    var att1, att2;
                    if (team == 100) {
                        for (var item of obj.listA) {
                            switch (att[0].toString()) {
                                case "lat":
                                    att1 = item.lat;
                                    break;
                                case "lng":
                                    att1 = item.lng;
                                    break;
                                case "population":
                                    att1 = item.population;
                                    break;
                                case "gpd":
                                    att1 = item.gpd;
                                    break;
                                case "hdi":
                                    att1 = item.hdi;
                                    break;
                                case "gini":
                                    att1 = item.gini;
                                    break;
                            }
                            switch (att[1].toString()) {
                                case "lat":
                                    att2 = item.lat;
                                    break;
                                case "lng":
                                    att2 = item.lng;
                                    break;
                                case "population":
                                    att2 = item.population;
                                    break;
                                case "gpd":
                                    att2 = item.gpd;
                                    break;
                                case "hdi":
                                    att2 = item.hdi;
                                    break;
                                case "gini":
                                    att2 = item.gini;
                                    break;
                            }
                            //console.log("LIST A "+item.country);
                            data.addRow([item.country, att1, att2, 'TeamA', item.population]);
                        }
                    } else {
                        for (var item of obj.listA) {
                            switch (att[0].toString()) {
                                case "lat":
                                    att1 = item.lat;
                                    break;
                                case "lng":
                                    att1 = item.lng;
                                    break;
                                case "population":
                                    att1 = item.population;
                                    break;
                                case "gpd":
                                    att1 = item.gpd;
                                    break;
                                case "hdi":
                                    att1 = item.hdi;
                                    break;
                                case "gini":
                                    att1 = item.gini;
                                    break;
                            }

                            switch (att[1].toString()) {
                                case "lat":
                                    att2 = item.lat;
                                    break;
                                case "lng":
                                    att2 = item.lng;
                                    break;
                                case "population":
                                    att2 = item.population;
                                    break;
                                case "gpd":
                                    att2 = item.gpd;
                                    break;
                                case "hdi":
                                    att2 = item.hdi;
                                    break;
                                case "gini":
                                    att2 = item.gini;
                                    break;
                            }
                            //console.log("LIST B "+item.country);
                            data.addRow([item.country, att1, att2, 'TeamB', item.population]);
                        }
                    }


                });
                var options = {
                    title: 'Countries and their Attributes ' +
                            'Enjoy',
                    hAxis: {title: att[0]},
                    vAxis: {title: att[1]},
                    'width': 550, 'height': 400,
                    bubble: {textStyle: {fontSize: 11}}
                };
                // Instantiate and draw the chart.
                var chart = new google.visualization.BubbleChart(document.getElementById('drawChart'));
                chart.draw(data, options);
            }
            //google.charts.setOnLoadCallback(drawChart);