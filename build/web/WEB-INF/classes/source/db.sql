/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  onelove
 * Created: Jul 6, 2019
 */

Create database web;

CREATE TABLE users (
    id int,
    first_name varchar(255),
    last_name varchar(255),
    email varchar(255),
    pwd varchar(255) 
);


CREATE TABLE countries_entered (
    id int,
    country varchar(255)
);

CREATE TABLE country_details (
    id int,
    Country varchar(255),
    Capital varchar(255),
    lat float,
    long_att float,
    Population int,
    GPD int,
    Gini int,
    HDI int,
    email varchar(255),
    pwd varchar(255) 
);