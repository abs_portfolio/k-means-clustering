
<%@page import="dbConnection.DbOperations"%>
<%@page import="java.util.ArrayList"%>

<%
    ArrayList<String> countries = null;
    ArrayList<String> attList = null;
    try {
        DbOperations db = new DbOperations();
        countries = db.getCountries();
        attList = db.getAttributes();
    } catch (ClassNotFoundException ex) {

    } catch (InstantiationException ex) {

    } catch (IllegalAccessException ex) {

    }
%>
<!DOCTYPE html>
<html>
    <head>
        <title>Web Programming 2018</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <link rel="stylesheet" href="style/auto.css">


        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>  
        <link rel="stylesheet" href="style/bootstrap.css" />
        <link rel="stylesheet" href="style/multiple-select.css" />
    </head>
    <body >

        <!-- Top container -->
        <div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
            <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
            <a href="logout.jsp" ><span  class="w3-bar-item w3-right">Log out</span> </a>
        </div>

        <!-- Sidebar/menu -->
        <nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px; background-color:#353434 !important; " id="mySidebar"><br>
            <div class="w3-container w3-row">
                <div class="w3-col s4">
                    <img src="pictures/university_logo.PNG" class="w3-circle w3-margin-right" style="width:46px">
                </div>
                <div class="w3-col s8 w3-bar">
                    <span>Welcome, <strong>hi</strong></span><br>
                </div>
            </div>
            <hr>
            <div class="w3-container">
                <h5>Dashboard</h5>
            </div>
            <div class="w3-bar-block">
                <a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Close Menu</a>
                <a href="autocompleteEx.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-bank fa-fw"></i>  General</a>
                <!--<a href="classification2.jsp" class=" w3-bar-item w3-button w3-padding"><i class="fa fa-users fa-fw"></i>  Classification</a>-->
                <!--<a href="dataSelect.jsp" class="w3-bar-item w3-button w3-padding w3-blue"><i class="fa fa-eye fa-fw"></i>  Charts</a>-->
                <a href="classification1.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-bullseye fa-fw"></i>    K-Means Clustering</a>               
            </div>
        </nav>


        <!-- Overlay effect when opening sidebar on small screens -->
        <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

        <!-- !PAGE CONTENT! -->
        <div class="w3-main" style="margin-left:300px;margin-top:43px;">

            <!-- Header -->
            <header class="w3-container" style="padding-top:22px">
                <div class="w3-container w3-row">
                    <h5><b><img src="pictures/university_logo.PNG" class="w3-circle w3-margin-right" style="width:46px"> My Dashboard</b></h5>
                    <hr>
                </div>
            </header>



            <div class="w3-panel">
                <div class="w3-row-padding" style="margin:0 -16px">
                    <div class="w3-container">
                        <div class="div-left" >
                            <form >
                                <h5>Select Countries</h5>
                                <select id ="countries"  multiple="multiple">
                                    <%for (int i = 0; i < countries.size(); i++) {
                                            out.print("<option value=" + countries.get(i).toString() + ">" + countries.get(i).toString() + "</option>\n");
                                        }%>
                                </select>
                                <hr>
                                <h5>Select Attribute</h5>
                                <select id="attribute">
                                    <%for (int i = 0; i < attList.size(); i++) {
                                            out.print("<option>" + attList.get(i).toString() + "</option>\n");
                                        }%>
                                </select>
                                <hr>
                                <h5>Select Chart</h5>
                                <select id="chart">
                                    <option value="PieChart">PieChart</option>

                                    <option value="columeChart">columeChart</option>

                                    <option value="pieHoleChart">pieHoleChart</option>

                                </select>
                            </form>
                        </div>
                        <div class="div-right" >
                            <button id="button"   class="w3-button w3-dark-grey">Generate Chart</button>
                            <hr>
                            <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                            <script type="text/javascript" src="./js/visualization-chart-script.js"></script>
                            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

                            <div id="drawChart"></div>
                        </div>
                    </div>
                </div>
                <!-- End page content -->
            </div>
        </div>
        <script src="js/multiple-select.js"></script>
        <script>
                    $(function () {
                        $('#countries').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    });
        </script>
        <script>
            // Get the Sidebar
            var mySidebar = document.getElementById("mySidebar");

            // Get the DIV with overlay effect
            var overlayBg = document.getElementById("myOverlay");

            // Toggle between showing and hiding the sidebar, and add overlay effect
            function w3_open() {
                if (mySidebar.style.display === 'block') {
                    mySidebar.style.display = 'none';
                    overlayBg.style.display = "none";
                } else {
                    mySidebar.style.display = 'block';
                    overlayBg.style.display = "block";
                }
            }

            // Close the sidebar with the close button
            function w3_close() {
                mySidebar.style.display = "none";
                overlayBg.style.display = "none";
            }
        </script>

    </body>
</html>
