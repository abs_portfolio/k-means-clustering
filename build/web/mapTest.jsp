<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Marker Animations</title>
        <style>
            /* Always set the map height explicitly to define the size of the div
             * element that contains the map. */
            #map {
                height: 100%;
            }
            /* Optional: Makes the sample page fill the window. */
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
        </style>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>  
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    </head>
    <body>
        <script>
            google.charts.load('current',
                    {
                        'packages': ['geochart'],
                        // Note: you will need to get a mapsApiKey for your project.
                        // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
                        'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
                    }
            );

            $(document).ready(function () {
                setInterval(function () {
                    myFun();
                }, 5000);
            });

            function myFun() {

                var Combined = new Array();
                var count = 0;
                $.ajax({
                    url: "AjaxServlet",
                    success: function (responseJson) {
                        $.each(responseJson, function (index, data) {    // Iterate over the JSON array.
                            console.log(data.count);
                            var team, centerLatA, centerLngA, centerLatB, centerLngB;

                            if (!isNaN(parseInt(data.team))) {
                                team = parseInt(data.team);
                            }
                            var vals = [];
                            for (var item of data.listA) {
                                vals.push(item.country);
                            }
                            for (var i = 0; i < vals.length; i++) {
                                Combined[count] = [vals[i], team];
                                count++;
                            }
                            if (parseInt(team) === 100) {
                                if (!isNaN(parseInt(data.centerLatA))) {
                                    centerLatA = parseFloat(data.centerLatA);
                                }
                                if (!isNaN(parseInt(data.centerLngA))) {
                                    centerLngA = parseFloat(data.centerLngA);
                                }
                            } else {
                                if (!isNaN(parseInt(data.centerLatB))) {
                                    centerLatB = parseFloat(data.centerLatA);
                                }
                                if (!isNaN(parseInt(data.centerLngB))) {
                                    centerLngB = parseFloat(data.centerLngA);
                                }
                            }
                        });

                        //google.charts.setOnLoadCallback(drawMe(Combined));
                        drawMe(Combined);
                    }
                });
            }
            function drawMe(Combined) {
                console.log("Sakafkasksd");
                drawMap(Combined);
            }
            function drawMap(Combined) {
                console.log(Combined);
                var data = new google.visualization.DataTable();
                // Add columns
                data.addColumn('string', 'Country');
                data.addColumn('number', 'Team');

                // Add empty rows
                data.addRows(Combined.length);
                for (var i = 0; i < Combined.length; i++) {
                    var cube = Combined[i];
                    for (var j = 0; j < cube.length; j++) {
                        data.setCell(i, j, cube[j]);
                    }
                }
                var options = {colorAxis: {colors: ['green', 'blue']}};
                options['dataMode'] = 'regions';

                var geomap = new google.visualization.GeoChart(document.getElementById('somediv'));

                geomap.draw(data, options);
            }


        </script>

        <div id="somediv"></div>
    </body>

</html>