# K-Means-clustering

This system is a K-Means clustering web-site that can request information about a country from Wikipedia.

The backed of this infrastructure was created using JSP technology. It also requires a MySQL database running instance. In this specific case MariaDB was used.

**Core elements.**

1. MySQL Database : This project requires a running instance of a MySQL database. Once we have a running instance of a MySQL database, we can run the script db.sql in order to create our database and its tables. Or you can just create the by copy-paste from the script to the db client. More specifically we need a database named : web , and tree tables named : users, country_details, countries_entered.
2. The back-end. The back-end is written in JSP. The servlets process the AJAX requests and responds accordingly.
3. The front-end is written in HTML/CSS/javascript. For the UI the w3 library were used which provides a simple graphical interface.
4. The communication between the HTML page and the backend is with AJAX requests written in javascript with no framework.

**Core use cases :**
1. Insert a countries name and request from the Wikipedia API its details.
2. Visualize some results through charts (not included in the live demo, only the code incl).
3. Classification & visualization countries information with google charts.
4. K-Means live clustering for the default cities on google maps templates.
5. User Login/Register/logout

There is a demo of this project, hosted on a Amazon Vm running Ubuntu 18.04 server addition. More specifically on a Tomee Aplication Server with a Maria DB hosting the mysql database.

project link : [cls_demo](http://52.47.144.179:8080/Cls_Demo/index.html?state=demo)

**Demo screenshots :****
*K-Means clustering.*
![GitHub Logo](./screenshots/geo_loc.png)

*Country details request from wiki.*
![GitHub Logo](./screenshots/wiki_search.png)


** Note that this project was originally 