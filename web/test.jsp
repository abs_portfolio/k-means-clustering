<!DOCTYPE html>
<html lang="en">
    <head>
        <title>SO question 4112686</title>
        <script src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script>
            $(document).ready(function () {
                setInterval(function () {
                    myFun();
                }, 5000);
                //console.log();
            });
            function myFun() {        // When HTML DOM "click" event is invoked on element with ID "somebutton", execute the following function...
                $.get("AjaxServlet", function (responseJson) {          // Execute Ajax GET request on URL of "someservlet" and execute the following function with Ajax response JSON...
                    var $table = $("<table>").appendTo($("#somediv")); // Create HTML <table> element and append it to HTML DOM element with ID "somediv".
                    $.each(responseJson, function (index, data) {    // Iterate over the JSON array.
                        console.log(data.count);
                        var team, centerLatA, centerLngA, centerLatB, centerLngB;
                        if (!isNaN(parseInt(data.team))) {
                            team = parseInt(data.team);
                        }
                        var vals = [];
                        for (var item of data.listA) {
                            vals.push(item.country);
                        }
                        //var parsed = JSON.parse(data.listA.toString());
                        if (parseInt(team) === 100) {
                            if (!isNaN(parseInt(data.centerLatA))) {
                                centerLatA = parseFloat(data.centerLatA);
                            }
                            console.log("TEAM A");
                            console.log(vals);
                            if (!isNaN(parseInt(data.centerLngA))) {
                                centerLngA = parseFloat(data.centerLngA);
                            }
                            $("<tr>").appendTo($table)                     // Create HTML <tr> element, set its text content with currently iterated item and append it to the <table>.
                                    .append($("<td>").text("TeamA count " + data.team + " = " + data.count))        // Create HTML <td> element, set its text content with id of currently iterated product and append it to the <tr>.
                                    .append($("<td>").text("centerLatA = " + centerLatA))      // Create HTML <td> element, set its text content with name of currently iterated product and append it to the <tr>.
                                    .append($("<td>").text("centerLatA = " + centerLngA));
                        } else {
                            if (!isNaN(parseInt(data.centerLatB))) {
                                centerLatB = parseFloat(data.centerLatA);
                            }
                            console.log("TEAM B");
                            console.log(vals);
                            if (!isNaN(parseInt(data.centerLngB))) {
                                centerLngB = parseFloat(data.centerLngA);
                            }
                            $("<tr>").appendTo($table)                     // Create HTML <tr> element, set its text content with currently iterated item and append it to the <table>.
                                    .append($("<td>").text("TeamB " + team + " count = " + data.count))        // Create HTML <td> element, set its text content with id of currently iterated product and append it to the <tr>.
                                    .append($("<td>").text("centerLatB = " + data.centerLatA))      // Create HTML <td> element, set its text content with name of currently iterated product and append it to the <tr>.
                                    .append($("<td>").text("centerLngB = " + data.centerLngA));
                        }
                    });
                });
            }
        </script>
    </head>
    <body>
        <div id="somediv"></div>
    </body>
</html>
