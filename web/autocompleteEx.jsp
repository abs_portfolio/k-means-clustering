<%@ page import ="java.sql.*,source.*" %>
<%
    String country = (String) request.getAttribute("country");
    String name = (String) request.getAttribute("userid");
%>
<!DOCTYPE html>
<html>
    <head>
        <title>Web Programming 2018</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
        </style>
        <script type="text/javascript" 
        src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script src="js/jquery.autocomplete.js"></script>  
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
        <script src="js/getWikiData.js" type="text/javascript"></script>
        <link rel="stylesheet" href="style/auto.css">
        <script type="text/javascript">
            $(document).ready(function () {
                var country = "<%=country%>";
            
                if (country === undefined || country === null) {
                    console.log("dadasd");
                    close_loader();
                }else {
                    console.log("daddasdasdasdasd");
                    open_loader();
                }
                console.log(country);
                document.getElementById("country").value = country;
                document.getElementById("DomObject").value = country;
                $('#article').wikiblurb();

            });
              function open_loader(){
               document.getElementById("article").innerHTML = '</br></br></br></br></br> <img src="./pictures/loader.gif" alt="Be patient..." height="150" width="150"/>';
            }
            function close_loader(){
               document.getElementById("article").innerHTML = '';
            }

            function getValues() {
                var rows = document.getElementsByClassName("infobox geography vcard")[0].rows;
                for (var i = rows.length - 1; i >= 0; i--) {
                    var row = rows[i];
                    var text = row.cells[0].innerText;
                    if (text.indexOf("Per capita") != -1) {//<\/?[a-z][a-z0-9]*[^<>]*>|<!--.*?-->img
                        var str = rows[i].cells[1].innerHTML;
                        var me = str.replace(/<\/?[a-z][a-z0-9]*[^<>]*>|<!--.*?-->img/gm,"");//deletes all html tags
                        me = me.substring(0, me.indexOf('('));//delete all after parenthesis
                        me = me.replace(/,/g , "");//replace all comas
                         me = me.replace("$" , "");//replace all comas�
                         me = me.replace("�" , "");//replace all comas�
                         me = me.replace(" " , "");//replace all comas�
                        document.getElementById("GDP").value = me;
                    } else if (text.indexOf("Population") != -1) {
                        var str = rows[i + 1].cells[1].innerHTML;
                        var me = str.replace(/<\/?[a-z][a-z0-9]*[^<>]*>|<!--.*?-->img/gm,"");//deletes all html tags
                        me = me.substring(0, me.indexOf('('));//delete all after parenthesis
                        me = me.replace(/,/g , "");//replace all comas
                        me = me.replace(" " , "");//replace all comas�
                        document.getElementById("population").value = me;
                    } else if (text.indexOf("Gini") != -1) {
                        var str = rows[i].cells[1].innerHTML;
                        var me = str.replace(/<\/?[a-z][a-z0-9]*[^<>]*>|<!--.*?-->img/gm,"");
                        var f = parseFloat(me.match(/-?(?:\d+(?:\.\d*)?|\.\d+)/)[0]);
                        document.getElementById("Gini").value = f;
                    } else if (text.indexOf("Capital") != -1) {
                        const regex = /<\/?[a-z][a-z0-9]*[^<>]*>|<!--.*?-->img/gm;
                        var str = rows[i].cells[1].innerHTML;
                        var me = str.replace(/<\/?[a-z][a-z0-9]*[^<>]*>|<!--.*?-->img/gm,"");
                        me = me.split("/").pop();
                        me = me.replace(" " , "");//replace all comas�
                        document.getElementById("Area").value = me;
                    }else if (text.indexOf("HDI") != -1) {
                        const regex = /<\/?[a-z][a-z0-9]*[^<>]*>|<!--.*?-->img/gm;
                        var str = rows[i].cells[1].innerHTML;
                        var me = str.replace(/<\/?[a-z][a-z0-9]*[^<>]*>|<!--.*?-->img/gm,"");
                        me = me.split("/").pop();
                        var f = parseFloat(me.match(/-?(?:\d+(?:\.\d*)?|\.\d+)/)[0]);
                        document.getElementById("HDI").value = f;
                    }
                }
            }

        </script>


    </head>
    <body >

        <!-- Top container -->
        <div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
            <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i> �Menu</button>
            <a ><strong>Web Programming 2018 powered by ICSD</strong> </a>
            <a href="logout.jsp" ><span  class="w3-bar-item w3-right">Log out</span> </a>
        </div>

        <!-- Sidebar/menu -->
        <nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px; background-color:#353434 !important; " id="mySidebar"><br>
            <div class="w3-container w3-row">
                <div class="w3-col s4">
                    <img src="pictures/university_logo.PNG" class="w3-circle w3-margin-right" style="width:46px">
                </div>
                <div class="w3-col s8 w3-bar">
                    <span>Welcome, <strong><c:out <%=name%>/></strong></span><br>
                </div>
            </div>
            <hr>
            <div class="w3-container">
                <h5>Dashboard</h5>
            </div>
            <div class="w3-bar-block">
                <a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>� Close Menu</a>
                <a href="autocompleteEx.jsp" class="w3-bar-item w3-button w3-padding w3-blue"><i class="fa fa-bank fa-fw"></i>� Country Search</a>
                <!--<a href="classification2.jsp" class=" w3-bar-item w3-button w3-padding"><i class="fa fa-users fa-fw"></i>� Classification</a>-->
                <!--<a href="dataSelect.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i>� Charts</a>-->
                <a href="classification1.jsp" class="w3-bar-item w3-button w3-padding"><i class="fa fa-bullseye fa-fw"></i>� K-Means Clustering </a>               
            </div>
        </nav>


        <!-- Overlay effect when opening sidebar on small screens -->
        <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

        <!-- !PAGE CONTENT! -->
        <div class="w3-main" style="margin-left:300px;margin-top:43px;">

            <!-- Header -->
            <header class="w3-container" style="padding-top:22px">

                <h5><b><img src="pictures/university_logo.PNG" class="w3-circle w3-margin-right" style="width:46px"> My Dashboard</b></h5>
            </header>



            <div class="w3-panel">
                <div class="w3-row-padding" style="margin:0 -16px">
                    <div class="w3-container">
                        <form method="POST" action="CountrySearch">
                            <h5>Country Search</h5>
                            <input type="text" id="country" name="country" value="" />
                            <button type="submit" value="Submit" onclick="CountrySearch" class="w3-button w3-dark-grey">Search<i class="fa fa-arrow-right"></i></button>
                            <script>
                                $("#country").autocomplete("getdata.jsp");
                            </script>
                        </form>
                        <br /> 

                        <div>
                            <hr>
                            <hr>
                            <div class="div-left" id="article"></div>

                            <div class="div-right">
                                <button type="button" value="Update" onclick="getValues();" class="w3-button w3-dark-grey">Update   <i class="fa fa-arrow-left"></i></button><br/>
                                <form action="SubmitCountryData" method="post"  >
                                    <br/>
                                    <label>Country </label><br/><input type="text" id ="DomObject"name="DomObject" value=""><br />
                                    <label>Area</label><br/><input type="text" id="Area" name="Area"><br />
                                    <label>Population</label><input type="text" name="population"id ="population"required><br />
                                    <label>GDP per capita</label><input type="text" id="GDP" name="GDP"required><br />
                                    <label>HDI (Human Development Index)</label><input type="text" id="HDI"name="HDI"required><br />
                                    <label>Gini Coefficient</label><input type="text" id ="Gini" name="Gini"required><br />
                                    <hr>
                                    <button type="submit" value="Submit"onclick="SubmitCountryData" class="w3-button w3-dark-grey">Register City<i class="fa fa-arrow-right"></i></button>
                                </form>

                            </div>
                        </div>
                    </div>

                    <hr>


                    <!-- End page content -->
                </div>
            </div>
        </div>
        <script>
            // Get the Sidebar
            var mySidebar = document.getElementById("mySidebar");

            // Get the DIV with overlay effect
            var overlayBg = document.getElementById("myOverlay");

            // Toggle between showing and hiding the sidebar, and add overlay effect
            function w3_open() {
                if (mySidebar.style.display === 'block') {
                    mySidebar.style.display = 'none';
                    overlayBg.style.display = "none";
                } else {
                    mySidebar.style.display = 'block';
                    overlayBg.style.display = "block";
                }
            }

            // Close the sidebar with the close button
            function w3_close() {
                mySidebar.style.display = "none";
                overlayBg.style.display = "none";
            }
        </script>

    </body>
</html>





