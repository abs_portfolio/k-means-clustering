
function check_pass() {
    if (document.getElementById('password').value == document.getElementById('confirm_password').value) {
			var str = document.getElementById('password').value;
			var strongRegex = new RegExp("^(?=.*[A-Z]{2,})(?=.*[$@#$!%*?&])(?=.*?\\d.*\\d)(?=.{6,})");
			
			if(strongRegex.test(str)){
			document.getElementById('submit').disabled = false;
		    document.getElementById('message').style.color = 'green';
			document.getElementById('message').innerHTML = 'acceptable';
			}else{
				  document.getElementById('submit').disabled = true;
		document.getElementById('message').style.color = 'red';
			document.getElementById('message').innerHTML = 'not acceptable';}
    } else {
        document.getElementById('submit').disabled = true;
		document.getElementById('message').style.color = 'red';
    document.getElementById('message').innerHTML = 'not matching';
    }
}

