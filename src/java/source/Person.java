/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package source;

/**
 *
 * @author OneLove
 */
public class Person {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private int personId;
    
    public Person(String firstName ,String lastName,String email,String password){
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }
    
    
    public Person(int personId,String firstName ,String lastName,String email,String password){
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.personId = personId;
    }
    
    public String getFirstName(){
        return this.firstName;
    }
    
    public String getLasttName(){
        return this.lastName;
    }
    
    public String getEmail(){
        return this.email;
    }
    
    public String getPassword(){
        return this.password;
    }
    
    public int getPersonId(){
        return this.personId;
    }
}
