/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package source;

/**
 *
 * @author OneLove
 */
public class Country {

    private int id;
    private String country;
    private String capitall;
    private float lat;
    private float lng;
    private int population;
    private int gpd;
    private double gini;
    private double hdi;

    public Country(int id,
            String country,
            String capitall,
            float lat,
            float lng,
            int population,
            int gpd,
            double gini,
            double hdi) {
        this.id = id;
        this.country = country;
        this.capitall = capitall;
        this.lat = lat;
        this.lng = lng;
        this.population = population;
        this.gpd = gpd;
        this.gini = gini;
        this.hdi = hdi;
    }

    public Country(String country,
            String capitall,
            float lat,
            float lng,
            int population,
            int gpd,
            double gini,
            double hdi) {
        this.country = country;
        this.capitall = capitall;
        this.lat = lat;
        this.lng = lng;
        this.population = population;
        this.gpd = gpd;
        this.gini = gini;
        this.hdi = hdi;
    }

    public int getId() {
        return this.id;
    }

    public String getCountry() {
        return this.country;
    }

    public String getCapital() {
        return this.capitall;
    }

    public float getLat() {
        return this.lat;
    }

    public float getLong() {
        return this.lng;
    }

    public int getPopulation() {
        return this.population;
    }

    public double getGini() {
        return this.gini;
    }

    public double getHdi() {
        return this.hdi;
    }

    public int getGpd() {
        return this.gpd;
    }
}
