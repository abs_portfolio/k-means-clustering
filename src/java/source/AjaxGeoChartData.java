/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package source;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author OneLove
 */
public class AjaxGeoChartData {

    private int team;
    private int count;
    private float centerLatA, centerLngA;
    private ArrayList<Country> listA;

    public AjaxGeoChartData(int team, int count,
            float centerLatA, float centerLngA, ArrayList<Country> listA) {
        this.team = team;
        this.count = count;
        this.centerLatA = centerLatA;
        this.centerLngA = centerLngA;
        //this.centerLatB = centerLatB;
        //this.centerLngB = centerLngB;
        this.listA = listA;
        //this.listB = listB;
    }

    public AjaxGeoChartData(int team,
            float centerLatA, float centerLngA, ArrayList<Country> listA) {
        this.team = team;
        this.count = count;
        this.centerLatA = centerLatA;
        this.centerLngA = centerLngA;
        //this.centerLatB = centerLatB;
        //this.centerLngB = centerLngB;
        this.listA = listA;
        //this.listB = listB;
    }

    public int getCount() {
        return this.count;
    }

    public ArrayList<Country> getCountriesA() {
        return this.listA;
    }

    public float getCenterLatA() {
        return this.centerLatA;
    }

    public int getTeam() {
        return this.team;
    }

    public float getCenterLngA() {
        return this.centerLngA;
    }

}
