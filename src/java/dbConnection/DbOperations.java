package dbConnection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import source.Country;
import source.Person;

/**
 *
 * @author OneLove
 */
public class DbOperations {

    private static Connection conn = null;

    public DbOperations() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        try {

            Class.forName("com.mysql.jdbc.Driver");
            String connectionUrl = "jdbc:mysql://52.47.144.179:3306/web?"
                    + "user=root&password=admin";
            conn = DriverManager.getConnection(connectionUrl);
            conn.setAutoCommit(false);
        } catch (SQLException ex) {
            Logger.getLogger(DbOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConnection() throws SQLException {
        conn.commit();
        conn.close();
    }

    public boolean inseartUser(Person user) {

        try {
            /*Stage one create the order*/
            String queryString = "INSERT INTO users (first_name,last_name,email,pwd ) VALUES ('" + user.getFirstName() + "','" + user.getLasttName() + "','" + user.getEmail() + "','" + user.getPassword() + "');";
            Statement st = conn.createStatement();
            st.executeUpdate(queryString);
            /*Stage one create the order*/

            closeConnection(); // chech this shit
            return true;
        } catch (SQLException ex) {
            System.err.println(ex.toString());
        }

        return false;
    }

    public boolean inseartCountry_details(String ccountry, float lat, float lng, int population, int gpd, double hdi, double gini, String capital) {

        try {
            /*
            INSERT INTO `country_details`( `Capital`, `lat`, `long_att`, `Population`, `GPD`, `HDI`, `Gini`) VALUES ('" + user.getFirstName() + "','" + user.getFirstName() + "','" + user.getFirstName() + "','" + user.getFirstName() + "','" + user.getFirstName() + "','" + user.getFirstName() + "','" + user.getFirstName() + "',);
            Stage one create the order*/
            String queryString = " INSERT INTO `country_details`(`Capital`, `lat`, `long_att`, `Population`, `GPD`, `HDI`, `Gini`, `Country`) VALUES ('" + capital + "'," + lat + "," + lng + "," + population + "," + gpd + "," + hdi + "," + gini + ",'" + ccountry + "')";
            Statement st = conn.createStatement();
            st.executeUpdate(queryString);
            /*Stage one create the order*/
            closeConnection(); // chech this shit
            return true;
        } catch (SQLException ex) {
            System.err.println(ex.toString());
        }

        return false;
    }

    public void inseartCountry(String country) {
        try {
            String queryString = "INSERT INTO countries_entered (country) VALUES ('" + country.toLowerCase() + "');";
            Statement st = conn.createStatement();
            st.executeUpdate(queryString);
            /*Stage one create the order*/

            closeConnection(); // chech this shit

        } catch (SQLException ex) {
            Logger.getLogger(DbOperations.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ArrayList<Country> getCntries() {

        ArrayList<Country> matched = new ArrayList<Country>();

        try {
            Statement stm = conn.createStatement();

            String sql = "Select * From country_details";
            ResultSet rst;
            rst = stm.executeQuery(sql);

            while (rst.next()) {
                matched.add(new Country(rst.getString("Country"), rst.getString("Capital"), rst.getFloat("lat"), rst.getFloat("long_att"), rst.getInt("Population"), rst.getInt("GPD"), rst.getInt("Gini"), rst.getInt("HDI")));
            }
            closeConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DbOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
        return matched;
    }

    public ArrayList<String> getCountries(String query) {
        ArrayList<String> matched = new ArrayList<String>();
        ArrayList<String> totalCountries = new ArrayList<String>();
        String country = null;
        query = query.toLowerCase();
        try {

            Statement stm = conn.createStatement();

            String sql = "Select * From countries_entered";
            ResultSet rst;
            rst = stm.executeQuery(sql);

            while (rst.next()) {
                totalCountries.add(rst.getString("country"));
            }

            for (int i = 0; i < totalCountries.size(); i++) {
                country = totalCountries.get(i).toLowerCase();
                if (country.startsWith(query)) {
                    matched.add(totalCountries.get(i));
                }
            }
            closeConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DbOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
        return matched;
    }

    public ArrayList<String> getCountries() {
        try {
            Statement stm = conn.createStatement();

            String sql = "Select * From country_details";
            ResultSet rst;
            rst = stm.executeQuery(sql);
            ArrayList<String> totalCountries = new ArrayList<String>();
            while (rst.next()) {
                totalCountries.add(rst.getString("country"));
            }

            if (totalCountries.size() < 2) {
                return null;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DbOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList<String> matched = new ArrayList<String>();

        try {

            Statement stm = conn.createStatement();

            String sql = "Select * From country_details";
            ResultSet rst;
            rst = stm.executeQuery(sql);

            while (rst.next()) {
                matched.add(rst.getString("Country"));
            }
closeConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DbOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
        return matched;
    }

    public boolean loginUser(Person user) {
        try {
            Statement stm = conn.createStatement();
            String sql = "Select * From users";
            ResultSet rst;
            rst = stm.executeQuery(sql);
            ArrayList<Person> users = new ArrayList<Person>();
            while (rst.next()) {
                Person prod = new Person(rst.getInt("id"), rst.getString("first_name"), rst.getString("last_name"), rst.getString("email"), rst.getString("pwd"));
                users.add(prod);
            }
            for (int i = 0; i < users.size(); i++) {
                if (user.getEmail().equals(users.get(i).getEmail()) && user.getPassword().equals(users.get(i).getPassword())) {
                    return true;
                }
            }
            closeConnection();
        } catch (SQLException ex) {
            System.err.println(ex.toString());
        }
        return false;
    }

    public ArrayList<String> getAttributes() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("lat");
        list.add("lng");
        list.add("population");
        list.add("gpd");
        list.add("hdi");
        list.add("gini");
        return list;
    }

    public List<Country> getCountries(String countries[]) {
        ArrayList<Country> matched = new ArrayList<Country>();

        try {
            for (int i = 0; i < countries.length; i++) {

                Statement stm = conn.createStatement();

                String sql = "Select * From country_details where Country = '" + countries[i].toString() + "'";
                ResultSet rst;
                rst = stm.executeQuery(sql);

                while (rst.next()) {
                    matched.add(new Country(rst.getString("Country"), rst.getString("Capital"), rst.getFloat("lat"), rst.getFloat("long_att"), rst.getInt("Population"), rst.getInt("GPD"), rst.getInt("Gini"), rst.getInt("HDI")));
                }

            }
            closeConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DbOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
        return matched;
    }

}
