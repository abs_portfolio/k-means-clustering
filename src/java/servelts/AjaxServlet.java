/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servelts;

import com.google.gson.Gson;
import dbConnection.DbOperations;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import source.*;

/**
 *
 * @author OneLove
 */
@WebServlet(name = "AjaxServlet", urlPatterns = {"/AjaxServlet"})
public class AjaxServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static int count;
    private static DbOperations db;
    private static List<Country> countriesToSend;
    private static float centerLatA, centerLatB, centerLngA, centerLngB;
    private static ArrayList<Country> listA;
    private static ArrayList<Country> listB;

    @Override
    public void init() {
        try {
            db = new DbOperations();
            countriesToSend = db.getCntries();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GetCountries.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(GetCountries.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(GetCountries.class.getName()).log(Level.SEVERE, null, ex);
        }
        initializeValues();
    }

    private void initializeCount() {
        count = 0;
    }

    private final static void initializeValues() {
        System.out.println("INITIALIZING VALUES");
        count = 0;

        listA = new ArrayList<Country>();
        listB = new ArrayList<Country>();
        Random rand = new Random();
        int centerA = rand.nextInt(countriesToSend.size());
        int centerB = rand.nextInt(countriesToSend.size());
        do {
            centerB = rand.nextInt(countriesToSend.size());
        } while (centerB == centerA);

        centerLatA = countriesToSend.get(centerA).getLat();
        centerLngA = countriesToSend.get(centerA).getLong();

        centerLatB = countriesToSend.get(centerB).getLat();
        centerLngB = countriesToSend.get(centerB).getLong();

    }

    public static float distance(Country country, float centerLat, float centerLong) {
        return (float) Math.sqrt(Math.pow(country.getLat() - centerLat, 2) + (Math.pow(country.getLong() - centerLong, 2)));
    }

    public AjaxServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ArrayList<AjaxGeoChartData> data = new ArrayList<AjaxGeoChartData>();
        for (int i = 0; i < countriesToSend.size(); i++) {
            System.out.println(listA.remove(countriesToSend.get(i)));
            System.out.println(listB.remove(countriesToSend.get(i)));
            if (distance(countriesToSend.get(i), centerLatA, centerLngA) < distance(countriesToSend.get(i), centerLatB, centerLngB)) {
                listA.add(countriesToSend.get(i));
            } else {
                listB.add(countriesToSend.get(i));
            }

        }
        //refinds center of A
        float tempLat = 0, tempLng = 0;
        for (int a = 0; a < listA.size(); a++) {
            System.out.println(a);
            tempLat += listA.get(a).getLat();
            tempLng += listA.get(a).getLong();
        }
        centerLatA = tempLat / listA.size();
        centerLngA = tempLng / listA.size();

        tempLat = 0;
        tempLng = 0;
        //refinds center of B
        for (int a = 0; a < listB.size(); a++) {
            tempLat += listB.get(a).getLat();
            tempLng += listB.get(a).getLong();
        }

        centerLatB = tempLat / listB.size();
        centerLngB = tempLng / listB.size();
        System.out.println("CenterLatA = " + centerLatA);
        System.out.println("centerLngA = " + centerLngA);
        System.out.println("CenterLatB = " + centerLatB);
        System.out.println("centerLngB = " + centerLngB);
        
        AjaxGeoChartData teamA = new AjaxGeoChartData(100, count, centerLatA, centerLngA, listA);
        AjaxGeoChartData teamB = new AjaxGeoChartData(800, count, centerLatB, centerLngB, listB);
        data.add(teamA);
        data.add(teamB);
        Gson gson = new Gson();
        String json = gson.toJson(data);
        count++;
        if (count > 5) {
            initializeValues();
        }
        response.setContentType("application/json");
        System.out.println(json);
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }
}
