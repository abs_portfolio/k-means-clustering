package servelts;

import com.google.gson.Gson;
import dbConnection.DbOperations;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import source.AjaxGeoChartData;
import source.Country;


@WebServlet(name = "AjaxClassification2", urlPatterns = {"/AjaxClassification2"})
public class AjaxClassification2 extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static DbOperations db;
    private static List<Country> defaultCountries, countriesProccess;
    private static float att1A, att1B, att2A, att2B;
    private static ArrayList<Country> listA;
    private static ArrayList<Country> listB;

    public static float distance(Country country, float att1center, float att2center) {
        return (float) Math.sqrt(Math.pow(getAtt(country, attribute[0]) - att1center, 2) + (Math.pow(getAtt(country, attribute[1]) - att2center, 2)));
    }

    @Override
    public void init() {
        try {
            db = new DbOperations();
            defaultCountries = db.getCntries();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GetCountries.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(GetCountries.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(GetCountries.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    
    private static float getAtt(Country country, String att) {

        switch (att) {
            case "lat": {
                return country.getLat();
            }
            case "lng": {
                return country.getLong();
            }
            case "population": {
                return country.getPopulation();
            }
            case "gpd": {
                return country.getGpd();
            }
            case "hdi": {
                return (float) country.getHdi();
            }
            case "gini": {
                return (float) country.getGini();
            }

        }
        return 0;
    }

    private final static void initializeValues() {
        System.out.println("INITIALIZING VALUES");
        listA = new ArrayList<Country>();
        listB = new ArrayList<Country>();
        Random rand = new Random();
        int centerA = rand.nextInt(defaultCountries.size());
        int centerB = rand.nextInt(defaultCountries.size());;
        do {
            centerB = rand.nextInt(defaultCountries.size());
        } while (centerB == centerA);

        att1A = getAtt(defaultCountries.get(centerA), attribute[0]);
        att2A = getAtt(defaultCountries.get(centerA), attribute[1]);

        att1B = getAtt(defaultCountries.get(centerB), attribute[0]);
        att2B = getAtt(defaultCountries.get(centerB), attribute[1]);
        System.out.println("STARTING VALUES");
        System.out.println("ATA1 = " + att1A);
        System.out.println("ATA2 = " + att2A);
        System.out.println("ATB1 = " + att1B);
        System.out.println("ATB2 = " + att1B);

    }

    public AjaxClassification2() {
        super();
    }
    private static String[] countries, attribute;

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        //defaultCountries = new ArrayList<Country>();
        countriesProccess = new ArrayList<Country>();

        countries = request.getParameterValues("countries[]");
        attribute = request.getParameterValues("att[]");

        for (int j = 0; j < countries.length; j++) {
            for (int i = 0; i < defaultCountries.size(); i++) {
                if (countries[j].equalsIgnoreCase(defaultCountries.get(i).getCountry())) {
                    countriesProccess.add(defaultCountries.get(i));
                }
            }
        }

        ArrayList<AjaxGeoChartData> data = new ArrayList<AjaxGeoChartData>();

        initializeValues();
        float previusAtt1A = att1A, previusAtt2A= att2A, previusAtt1B = att1B, previusAtt2B = att2B;
        int repeated = 0;
        for (int j = 0; j < 10; j++) {
            for (int i = 0; i < countriesProccess.size(); i++) {
                //if(j>2 && att1)
                System.out.println(listA.remove(countriesProccess.get(i)));
                System.out.println(listB.remove(countriesProccess.get(i)));
                if (distance(countriesProccess.get(i), att1A, att2A) < distance(countriesProccess.get(i), att1B, att2B)) {
                    listA.add(countriesProccess.get(i));
                } else {
                    listB.add(countriesProccess.get(i));
                }

            }
            //refinds center of A
            float tempAtt1 = 0, tempAtt2 = 0;
            for (int a = 0; a < listA.size(); a++) {
                System.out.println(a);
                tempAtt1 += getAtt(listA.get(a), attribute[0]);
                tempAtt2 += getAtt(listA.get(a), attribute[1]);
            }
            if (listA.size() != 0) {
                att1A = tempAtt1 / listA.size();
                att2A = tempAtt2 / listA.size();
            }
            tempAtt1 = 0;
            tempAtt2 = 0;
            //refinds center of B
            for (int a = 0; a < listB.size(); a++) {
                tempAtt1 += getAtt(listB.get(a), attribute[0]);
                tempAtt2 += getAtt(listB.get(a), attribute[1]);
            }

            if (listB.size() != 0) {
                att2B = tempAtt2 / listB.size();
                att1B = tempAtt1 / listB.size();
            }
            //ifcenters have the same  possition as the previus repeat . In simpler words if they dont move for 2 repeats break the for
            if(previusAtt1A == att1A && previusAtt2A == att2A && previusAtt1B == att1B && previusAtt2B == att2B ){  
                if(repeated > 1)
                    break;
                else repeated++;
            }else repeated =0;
                
        }

        Gson gson = new Gson();

        AjaxGeoChartData teamA = new AjaxGeoChartData(100, att1A, att2A, listA);
        AjaxGeoChartData teamB = new AjaxGeoChartData(800, att1B, att2B, listB);
        System.out.println("ATA1 = " + att1A);
        System.out.println("ATA2 = " + att2A);
        System.out.println("ATB1 = " + att1B);
        System.out.println("ATB2 = " + att1B);
        data.add(teamA);
        data.add(teamB);
        String jsonString = gson.toJson(data);
        response.setContentType("application/json");
        System.out.println(jsonString);
        response.getWriter().write(jsonString);

    }

}
