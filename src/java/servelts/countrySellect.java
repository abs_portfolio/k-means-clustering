/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servelts;

import com.google.gson.Gson;
import dbConnection.DbOperations;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import source.Country;

/**
 *
 * @author OneLove
 */
@WebServlet(name = "countrySellect", urlPatterns = {"/countrySellect"})
public class countrySellect extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<String> countries = null;
        ArrayList<String> attList = null;
        try {
            DbOperations db = new DbOperations();
            countries = db.getCountries();
            attList = db.getAttributes();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(countrySellect.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(countrySellect.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(countrySellect.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>\n"
                    + "<!--\n"
                    + "To change this license header, choose License Headers in Project Properties.\n"
                    + "To change this template file, choose Tools | Templates\n"
                    + "and open the template in the editor.\n"
                    + "-->\n"
                    + "<html>\n"
                    + "    <head>\n"
                    + "        <title>Chart Page</title>\n"
                    + "        <meta charset=\"UTF-8\">\n"
                    + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                    + "        <link rel=\"stylesheet\" href=\"style/taskbar.css\">\n"
                    + "        <script src=\"js/dropDown.js\"></script> \n"
                    + "        <link rel=\"stylesheet\" href=\"style/dropDown.css\">\n"
                    + " <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n"
                    + "    </head>\n"
                    + "    <body>\n"
                    + "\n"
                    + "        <ul>\n"
                    + "            <li><a href=\"#home\">Home</a></li>\n"
                    + "            <li><a  href=\"autocompleteEx.jsp\">Country Search</a></li>\n"
                    + "            <li><a   class=\"active\" href=\"countrySellect\">Data Sellect</a></li>\n"
                    + "            <li><a href=\"logout.jsp\">log out</a></li>\n"
                    + "        </ul>\n"
                    + "\n");
            try {
                out.println("        <form method=\"POST\" action=\"countrySellect\">\n");
                out.println("<select name =\"countries\" MULTIPLE>\n"
                        + "      <option>Select an option</option>\n");
                for (int i = 0; i < countries.size(); i++) {
                    out.println("<option>" + countries.get(i).toString() + "</option>\n");
                }
                out.println("</select>\n"
                        + "<div id=\"checkboxes\">\n");

            } catch (NullPointerException ex) {
                out.println("<h1>YOu need more than 10 countries in order to access this action</h1>");
            }
            out.println("                </div>\n"
                    + "            </div>\n");
            out.println();
            out.println("<select name=\"attribute\">\n"
                    + "<option value=\"none\">Please Select the atrribute</option>\n");
            for (int i = 0; i < attList.size(); i++) {
                out.println("<option value=\"" + attList.get(i) + "\">" + attList.get(i) + "</option>\n");
            }
            out.println("</select>");
            out.println("<select name=\"chart\">\n"
                    + "<option value=\"none\">Please Select the chart</option>\n");

            out.println("<option value=\"one\">Pizza</option>\n");

            out.println("<option value=\"tow\">chart2</option>\n");

            out.println("<option value=\"three\">CHART3</option>\n");

            out.println("</select>");
            out.println("<input type=\"submit\" value=\"Create\" onclick=\"window.location = 'countrySellect';\"/>");
            out.println("</form>\n");
            
            try {
                String chart = request.getParameter("chart");
                String attribute = request.getParameter("attribute");
                countriez = request.getParameterValues("countries");
                for (int i = 0; i < countries.size(); i++) {
                    out.println("<h1>" + chart + "      " + attribute + "   " + countriez[i] + " </h1>");
                }
                out.println("<input type=\"button\" value=\"Capacity Chart\" id=\"DoMe\" >");
                out.println("<div id=\"student-bar-chart\"></div>\n"
                        + "		\n"
                        + "		<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js\"></script>\n"
                        + "		<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>\n"
                        + "		<script type=\"text/javascript\" src=\"js/visualization-chart-script.js\"></script>");
            } catch (NullPointerException ex) {
                out.println("<h1>Please sellect values and click submit</h1>");
            }
            out.println("</body>\n"
                    + "</html>\n");
        }
    }
    String[] countriez;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        List<Country> countries = null;
        try{
        try {
            DbOperations db = new DbOperations();
            countries = db.getCountries(countriez);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(countrySellect.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(countrySellect.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(countrySellect.class.getName()).log(Level.SEVERE, null, ex);
        }

        Gson gson = new Gson();

        String jsonString = gson.toJson(countries);

        response.setContentType("application/json");

        response.getWriter().write(jsonString);
        }catch(NullPointerException ex){
            processRequest(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
