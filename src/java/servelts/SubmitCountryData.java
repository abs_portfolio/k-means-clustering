/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servelts;

import dbConnection.DbOperations;
import java.io.*;
import java.util.Scanner;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author OneLove
 */
@WebServlet(name = "SubmitCountryData", urlPatterns = {"/SubmitCountryData"})
public class SubmitCountryData extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    //private final static Pattern CLIP_AMMO = Pattern.compile("*.(\\d+),(\\d+).*");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String area = (String) request.getParameter("Area");
        String country = (String) request.getParameter("DomObject");
        int gpd = (int) Integer.parseInt((String) request.getParameter("GDP"));
        double hdi = (double) Double.parseDouble((String) request.getParameter("HDI"));
        double gini = (double) Double.parseDouble((String) request.getParameter("Gini"));
        int population = (int) Integer.parseInt((String) request.getParameter("population"));
        float lat, lng;
        String capital = "Athens";
        String[] numbers = area.split(";");
        lat = Float.parseFloat(numbers[0]);
        lng = Float.parseFloat(numbers[1]);
        System.out.println("LAAATTTT : " + lat);
        try {
            DbOperations db = new DbOperations();
            db.inseartCountry_details(country, lat, lng, population, gpd, hdi, gini,capital);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SubmitCountryData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(SubmitCountryData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(SubmitCountryData.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("LONGGG : " + lng);
      
        response.sendRedirect("autocompleteEx.jsp");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
