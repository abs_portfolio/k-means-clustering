package servelts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import source.Country;
import dbConnection.DbOperations;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/StudentJsonDataServlet")
public class GetCountries extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public GetCountries() {
        super();
    }

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        DbOperations db;
        List<Country> countriesToSend = null;
        String[] countries = request.getParameterValues("countries[]");
        String attribute = request.getParameter("att");

        System.out.println(countries[0]);
        System.out.println(attribute);

        try {
            db = new DbOperations();
            countriesToSend = db.getCountries(countries);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GetCountries.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(GetCountries.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(GetCountries.class.getName()).log(Level.SEVERE, null, ex);
        }

        Gson gson = new Gson();

        String jsonString = gson.toJson(countriesToSend);

        response.setContentType("application/json");
        System.out.println(jsonString);
        response.getWriter().write(jsonString);

    }

}
